/*********************************************************************
 *
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, Johannes Maurer,
 *                      Institute for Software Technology,
 *                      Graz University of Technology
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Graz University of Technology nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************/

#include <tug_mobile_robots/mobile_robots_node.hpp>
#include <math.h>
namespace tug_mobile_robots
{

MobileRobotsNode::MobileRobotsNode() :
    nh_(),
    step_count_(0),
    mode_(0)
{

}

MobileRobotsNode::~MobileRobotsNode()
{

}

void MobileRobotsNode::init()
{
    set_ellipse_srv_ = nh_.advertiseService("set_ellipse", &MobileRobotsNode::setEllipseCB, this);

    set_robot_pose_client_ =  nh_.serviceClient<tug_stage_ros::SetRobotPose>("set_robot_pose");
    cmd_vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    wheel_cmd_vel_pub_ = nh_.advertise<tug_stage_ros::WheelCmdVel>("wheel_cmd_vel", 10);
}

void MobileRobotsNode::step()
{
    switch(mode_)
    {
    case SetEllipseRequest::POSITION_MODE:
        if(step_count_ < robot_poses_.size())
        {
            Vector3d pose = robot_poses_[step_count_];

            tug_stage_ros::SetRobotPose srv;
            srv.request.x = pose[0];
            srv.request.y = pose[1];
            srv.request.yaw = pose[2];

            ROS_INFO_STREAM("set_robot_pose: x=" << srv.request.x << ", y=" << srv.request.y << ", yaw=" << srv.request.yaw);

            if(!set_robot_pose_client_.call(srv))
            {
              ROS_ERROR("Failed to call service set_robot_pose");
            }

            step_count_++;
        }
        break;
    case SetEllipseRequest::VELOCITY_MODE:
        if(step_count_ < robot_vels_.size())
        {
            Vector2d robot_vel = robot_vels_[step_count_];

            geometry_msgs::Twist msg;
            msg.linear.x = robot_vel[0];
            msg.angular.z = robot_vel[1];

            ROS_INFO_STREAM("cmd_vel: trans=" << msg.linear.x << ", rot=" << msg.angular.z);

            cmd_vel_pub_.publish(msg);

            step_count_++;
        }
        break;
    case SetEllipseRequest::WHEEL_MODE:
        if(step_count_ < robot_wheel_vels_.size())
        {
            Vector2d wheel_vel = robot_wheel_vels_[step_count_];

            tug_stage_ros::WheelCmdVel msg;
            msg.left = wheel_vel[0];
            msg.right = wheel_vel[1];

            wheel_cmd_vel_pub_.publish(msg);

            ROS_INFO_STREAM("wheel_cmd_vel: left=" << msg.left << ", right=" << msg.right);

            step_count_++;
        }
        break;
    default:
        ROS_ERROR("Invalid operation mode.");
    }
}


bool MobileRobotsNode::setEllipseCB(SetEllipseRequest &request, SetEllipseResponse &response)
{
    ROS_INFO_STREAM("modedd: " << request.mode);
    ROS_INFO_STREAM("m: x=" << request.m.x << ", y=" << request.m.y);
    ROS_INFO_STREAM("a: x=" << request.a.x << ", y=" << request.a.y);
    ROS_INFO_STREAM("b: x=" << request.b.x << ", y=" << request.b.y);

    mode_ = request.mode;
    step_count_ = 0;


    robot_poses_.clear();


    // TODO: compute robot position and heading

    Vector3d pose;
    for(int i=0;i<=600;i++) {
        pose[0] = request.m.x + request.a.x * cos((2 * M_PI / 600) * i) +
                  request.b.y * sin((2 * M_PI / 600) * i);  // x
        pose[1] = request.m.y + request.a.y * cos((2 * M_PI / 600) * i) +
                  request.b.x * sin((2 * M_PI / 600) * i);  // y

        pose[2] = atan2(pose[1], pose[0]);          // yaw

        robot_poses_.push_back(pose);
    }

    robot_vels_.clear();

    // TODO: compute robot linear velocity and angular velocity

    Vector2d vel;

    for(int i=0;i<=600;i++) {
        double x = request.m.x + request.a.x * cos((2 * M_PI / 600) * i) +
                  request.b.y * sin((2 * M_PI / 600) * i);  // x
        double y = request.m.y + request.a.y * cos((2 * M_PI / 600) * i) +
                  request.b.x * sin((2 * M_PI / 600) * i);  // y

        double dx=(2 * M_PI/30 )*(cos((2 * M_PI / 600) * i)*request.b.y-sin((2 * M_PI / 600) * i)*request.a.x);
        double dy=(2 * M_PI /30)*(cos((2 * M_PI / 600) * i)*request.b.x-sin((2 * M_PI / 600) * i)*request.a.y);
        double sqr=sqrt(dx*dx+dy*dy);
        vel[0] = sqr;  // trans

        double dx2=pow((2 * M_PI/30 ),2)*(-sin((2 * M_PI / 600) * i)*request.b.y-cos((2 * M_PI / 600) * i)*request.a.x);
        double dy2=pow((2 * M_PI/30 ),2)*(-sin((2 * M_PI / 600) * i)*request.b.x-cos((2 * M_PI / 600) * i)*request.a.y);
        vel[1] =(dx*dy2-dx2*dy)/(dx*dx+dy*dy) ; // rot

        robot_vels_.push_back(vel);
    }


    robot_wheel_vels_.clear();

    // TODO: compute left and right wheel velocity

    Vector2d wheel_vel;
    for(int i=0;i<=600;i++) {
        double x = request.m.x + request.a.x * cos((2 * M_PI / 600) * i) +
                   request.b.y * sin((2 * M_PI / 600) * i);  // x
        double y = request.m.y + request.a.y * cos((2 * M_PI / 600) * i) +
                   request.b.x * sin((2 * M_PI / 600) * i);  // y

        double dx=(2 * M_PI/30 )*(cos((2 * M_PI / 600) * i)*request.b.y-sin((2 * M_PI / 600) * i)*request.a.x);
        double dy=(2 * M_PI /30)*(cos((2 * M_PI / 600) * i)*request.b.x-sin((2 * M_PI / 600) * i)*request.a.y);
        double velocity=sqrt(dx*dx+dy*dy);
        double dx2=pow((2 * M_PI/30 ),2)*(-sin((2 * M_PI / 600) * i)*request.b.y-cos((2 * M_PI / 600) * i)*request.a.x);
        double dy2=pow((2 * M_PI/30 ),2)*(-sin((2 * M_PI / 600) * i)*request.b.x-cos((2 * M_PI / 600) * i)*request.a.y);

        double kappa =(dx*dy2-dx2*dy)/(pow((dx*dx+dy*dy),3.0/2)); // curvature

        wheel_vel[0] = velocity*(1-(0.15*kappa))/0.075;  // left wheel
        wheel_vel[1] = velocity*(1+(0.15*kappa))/0.075; // right wheel

        robot_wheel_vels_.push_back(wheel_vel);

    }


    return true;
}

} // end namespace tug_mobile_robots

int main(int argc, char** argv)
{

    try
    {
        ros::init(argc,argv,"mobile_robots_node");

        tug_mobile_robots::MobileRobotsNode mobile_robots_node;

        mobile_robots_node.init();

        ros::Rate loop_rate(20); // 20Hz
        while(ros::ok())
        {
            ros::spinOnce();
            mobile_robots_node.step();
            loop_rate.sleep();
        }
    }
    catch(...)
    {
        ROS_ERROR("Unhandled exception!");
        return -1;
    }

    return 0;
}
