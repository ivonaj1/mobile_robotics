/*********************************************************************
 *
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2014, Johannes Maurer,
 *                      Institute for Software Technology,
 *                      Graz University of Technology
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Graz University of Technology nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************/

#include <ros/ros.h>
#include <tug_mobile_robots/SetEllipse.h>

int main(int argc, char** argv)
{

    try
    {
        ros::init(argc,argv,"mobile_robots_test");

        if (argc != 2)
        {
            ROS_INFO("usage: mobile_robots_test mode");
            ROS_INFO("       mode = 0 - position");
            ROS_INFO("       mode = 1 - wheels");
            ROS_INFO("       mode = 2 - velocity");
            return 1;
        }

        ros::NodeHandle nh;

        ros::ServiceClient client = nh.serviceClient<tug_mobile_robots::SetEllipse>("set_ellipse");
        tug_mobile_robots::SetEllipse srv;
        srv.request.mode = atoi(argv[1]);

        srv.request.m.x = 0.0;
        srv.request.m.y = 0.0;
        srv.request.m.z = 0.0;

        srv.request.a.x = 5.0;
        srv.request.a.y = 0.0;
        srv.request.a.z = 0.0;

        srv.request.b.x = 2.5;
        srv.request.b.y = 0.0;
        srv.request.b.z = 0.0;

        if(!client.call(srv))
        {
            ROS_ERROR("Failed to call service mobile_robots_test");
            return 1;
        }

    }
    catch(...)
    {
        ROS_ERROR("Unhandled exception!");
        return -1;
    }

    return 0;
}
