#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/tf.h>
#include <math.h>
#include <sensor_msgs/PointCloud.h>

tf::Matrix3x3 rot_mat_stat(1,0,0,0,cos(M_PI),-sin(M_PI),0,sin(M_PI),cos(M_PI));
tf::Vector3 translation_vec_stat(0.0, 0.0, 0.15);
tf::Matrix3x3 rot_mat_dyn;
tf::Vector3 translation_vec_dyn;
ros::Publisher pointcloud_pub;

tf::Transform B;

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
float q0=msg->pose.pose.orientation.w;
float q1=msg->pose.pose.orientation.x;
float q2=msg->pose.pose.orientation.y;
float q3=msg->pose.pose.orientation.z;
float a11=q0*q0+q1*q1-q2*q2-q3*q3;
float a12=2*(q1*q2-q0*q3);
float a13=2*(q1*q3+q0*q2);
float a21=2*(q1*q2+q0*q3);
float a22=q0*q0-q1*q1+q2*q2-q3*q3;
float a23=2*(q2*q3-q0*q1);
float a31=2*(q1*q3-q0*q2);
float a32=2*(q2*q3+q0*q1);
float a33=q0*q0-q1*q1-q2*q2+q3*q3;

rot_mat_dyn.setValue(a11,a12,a13,a21,a22,a23,a31,a32,a33);
translation_vec_dyn.setValue(msg->pose.pose.position.x, msg->pose.pose.position.y, msg->pose.pose.position.z);
B.setOrigin(translation_vec_dyn);
B.setBasis(rot_mat_dyn);

tf::Transform B2;
B2.setOrigin(translation_vec_stat);
B2.setBasis(tf::Matrix3x3(1,0,0,0,1,0,0,0,1));

tf::Transform B3;
B3.setOrigin(tf::Vector3(0.05,0,0));
B3.setBasis(rot_mat_stat);
B *= B2;
B *= B3;
}

void base_scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
ros::NodeHandle n;
sensor_msgs::PointCloud cloud;
cloud.header.stamp = ros::Time::now();
cloud.header.frame_id = "odom";
ros::Publisher pointcloud_pub = n.advertise<sensor_msgs::PointCloud>("point_cloud", 1000);
cloud.points.resize(msg->ranges.size());

for (int i = 0; i < msg->ranges.size();i++)
{
    float range = msg->ranges[i];
    float angle  = msg->angle_min +(i * msg->angle_increment);
    tf::Vector3 laser_point(range*cos(angle), range*sin(angle), 0.0);
    laser_point =  B.getBasis()*laser_point + B.getOrigin();

  if(ros::ok())
  {
    
    cloud.points[i].x =laser_point.getX();
    cloud.points[i].y =laser_point.getY();
    cloud.points[i].z =laser_point.getZ();
    ros::spinOnce();
  }
}
pointcloud_pub.publish(cloud);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n1;
  ros::NodeHandle n2;
  ros::NodeHandle n3;

 pointcloud_pub = n3.advertise<sensor_msgs::PointCloud>("point_cloud", 1000);
 ros::Subscriber sub_odom= n1.subscribe("odom", 1000, odomCallback);
 ros::Subscriber sub_baseScan = n2.subscribe("base_scan", 1000, base_scanCallback);

  ros::spin();

  return 0;
}
