/*********************************************************************
 *
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Hermann Steffan
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * All advertising materials mentioningfeatures or use of this
 *     software must display the following acknowledgement: “This product
 *     includes software developed by Graz University of Technology and
 *     its contributors.”
 *   * Neither the name of Graz University of Technology nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *********************************************************************/

#include <tug_simple_turtle/simple_turtle.h>
#include<limits>
#include<tf/tf.h>
#include<tf/transform_listener.h>
#include<algorithm>
#define PI 3.14


bool overflow = false;


namespace SimpleTurtle
{

SimpleTurtle::SimpleTurtle() :
    nh_(),
    linear_(0.0),
    angular_(0.0),
    button_0_pressed_(false),
    occupancy_grid_map_(nh_),

    ticks_old_left_(0),
    ticks_old_right_(0),

    wheel_base_(0.23),
    wheel_rad_(0.035),
    tick_per_mm_(11.7),

    previous_x_(0.0),
    previous_y_(0.0),
    previous_theta_(0.0),
    x_ (0.0),
    y_ (0.0),
    theta_ (0.0),
    first_ (true),
    s_ (0.0),

    state1_ (true),
    state2_ (false),
    state3_(false),
    state4_(false),

    x_cw_cg_(0.4210596),
    x_ccw_cg_ (-0.2633),
    y_cw_cg_(0.2065),
    y_ccw_cg_(0.241)
{
}

SimpleTurtle::~SimpleTurtle()
{
}

void SimpleTurtle::init()
{

    double alpha1 = (x_cw_cg_ + x_ccw_cg_) / (-4.0);
    double alpha2 = (y_cw_cg_ - y_ccw_cg_) / (-4.0);

    double beta1 = (x_cw_cg_ - x_ccw_cg_) / (-4.0);
    double beta2 = (y_cw_cg_ + y_ccw_cg_) / (-4.0);

    double R = (1.0/2.0) / sin(beta1/2.0);
    double Ed = (R +(wheel_base_/2.0)) / (R -(wheel_base_/2.0));


    Dl_ = 2.0 / (Ed + 1)*wheel_rad_;
    Dr_ = 2.0 / (1.0/Ed + 1)*wheel_rad_;
    wheel_base_actual_ = ((PI/2) / (PI/2 - alpha1)) * wheel_base_;

    occupancy_grid_map_.init(2048, 2048, 0.05, -51.2, -51.2);

    bumper_sub_ = nh_.subscribe<kobuki_msgs::BumperEvent>("/mobile_base/events/bumper", 10, &SimpleTurtle::bumberCallback, this);
    button_sub_ = nh_.subscribe<kobuki_msgs::ButtonEvent>("/mobile_base/events/button", 10, &SimpleTurtle::buttonCallback, this);
    infrared_sub_ = nh_.subscribe<kobuki_msgs::DockInfraRed>("/mobile_base/events/", 10, &SimpleTurtle::dockingCallback, this);

    // TODO: subscribe to the laser scan
    laser_scan_sub_ = nh_.subscribe<sensor_msgs::LaserScan>("/scan", 10, &SimpleTurtle::laserScanCallback, this);

    sensor_state_sub_ = nh_.subscribe("/mobile_base/sensors/core", 10, &SimpleTurtle::sensorStateCallback, this);
    vel_pub_ = nh_.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/teleop", 1);
}

// publish cmd_vel
void SimpleTurtle::pubCmdVel(double linear, double angular)
{
    geometry_msgs::Twist vel;
    vel.angular.z = angular;
    vel.linear.x = linear;

    vel_pub_.publish(vel);
}

// Bumper
void SimpleTurtle::bumberCallback(const kobuki_msgs::BumperEvent::ConstPtr& msg)
{
    ROS_INFO("Bumper");

    switch(msg->state)
    {
    case kobuki_msgs::BumperEvent::CENTER:
        ROS_INFO("Center");
        break;
    case kobuki_msgs::BumperEvent::LEFT:
        ROS_INFO("Left");
        break;
    case kobuki_msgs::BumperEvent::RIGHT:
        ROS_INFO("Right");
        break;

    }
}


// Buttons
void SimpleTurtle::buttonCallback(const kobuki_msgs::ButtonEvent::ConstPtr& msg)
{
    ROS_INFO("Buttons");
    switch(msg->button)
    {
    case kobuki_msgs::ButtonEvent::Button0:
        ROS_INFO("Button 0");
        button_0_pressed_ = true;
        break;
    case kobuki_msgs::ButtonEvent::Button1:
        ROS_INFO("Button 1");
        break;
    case kobuki_msgs::ButtonEvent::Button2:
        ROS_INFO("Button 2");
        break;
    }
}


// Docking sensor
void SimpleTurtle::dockingCallback(const kobuki_msgs::DockInfraRed::ConstPtr& msg)
{
    ROS_INFO("Docking");
}

// Sensor State
void SimpleTurtle::sensorStateCallback(const kobuki_msgs::SensorState::ConstPtr& msg)
{
   // ROS_INFO_STREAM("left encoder= " << msg->left_encoder);
   // ROS_INFO_STREAM("right encoder= " << msg->right_encoder);


    if(first_)
    {
        first_ = false;
        ticks_old_left_ = msg->left_encoder;
        ticks_old_right_ = msg->right_encoder;
    }

    int current_left = msg->left_encoder;
    int current_right = msg->right_encoder;


    //ROS_INFO_STREAM("old left= " <<(ticks_old_left_));
    //ROS_INFO_STREAM("old right= " << (ticks_old_right_));


    int absolute_left = abs((current_left - ticks_old_left_));
    int absolute_right = abs((current_right - ticks_old_right_));

   // ROS_INFO_STREAM("abs left= " <<absolute_left);
   // ROS_INFO_STREAM("abs right= " << absolute_right);


    if(absolute_left > std::numeric_limits<uint16_t>::max()/2.0)
    {
        if(current_left < ticks_old_left_ )
        {
            // overflow
            ticks_old_left_  = std::numeric_limits<uint16_t>::max() - ticks_old_left_;
        }
        else
         {
            //underflow
           ticks_old_left_ = ticks_old_left_ +  std::numeric_limits<uint16_t>::max();
        }
    }

    if(absolute_right > std::numeric_limits<uint16_t>::max()/2.0)
    {
        if(current_right < ticks_old_right_ )
        {
            // overflow
            ticks_old_right_ = std::numeric_limits<uint16_t>::max() - ticks_old_right_;
        }
        else
        {
            //underflow
            ticks_old_right_ = ticks_old_right_ + std::numeric_limits<uint16_t>::max();
        }
    }

   // ROS_INFO_STREAM("max= " <<std::numeric_limits<uint16_t>::max());
   // ROS_INFO_STREAM("left current= " <<current_left);
   // ROS_INFO_STREAM("right current= " << current_right);

    int delta_left = current_left - ticks_old_left_;
    int delta_right = current_right - ticks_old_right_;

    float n=2*PI*0.035*11.7;
    float ticks_l=n/(2*PI*Dl_);
    float ticks_r=n/(2*PI*Dr_);

    //ROS_INFO_STREAM("ticks left= " << ticks_l);
//ROS_INFO_STREAM("ticks right= " << ticks_r);

    // with correction
    //float distance_left = ((delta_left / ticks_l)) / 1000;
    //float distance_right = ((delta_right / ticks_r)) / 1000;

    // without correction
    float distance_left = ((delta_left / tick_per_mm_)) / 1000;
    float distance_right = ((delta_right / tick_per_mm_)) / 1000;

    float delta_s = (distance_left + distance_right) / 2;

    // with correction
    //float delta_theta = (distance_right - distance_left) / wheel_base_actual_;
    float delta_theta = (distance_right - distance_left) / wheel_base_;


    x_ = previous_x_ + delta_s*cos(previous_theta_ + delta_theta/2.0);
    y_ = previous_y_ + delta_s*sin(previous_theta_ + delta_theta/2.0);
    theta_ = previous_theta_ + delta_theta;


    /*ROS_INFO_STREAM("delta_s= " << delta_s);
    ROS_INFO_STREAM("delta_theta= " << delta_theta);


    ROS_INFO_STREAM("x= " << x_);
    ROS_INFO_STREAM("y= " << y_);
    ROS_INFO_STREAM("theta= " << theta_);

    ROS_INFO_STREAM("delta_keft= " << delta_left);
    ROS_INFO_STREAM("delta_right= " << delta_right);

    ROS_INFO_STREAM("distance_keft= " << distance_left);
    ROS_INFO_STREAM("distance_right= " << distance_right);


    s_+=delta_s;
    ROS_INFO_STREAM("s= " << s_);
*/

    previous_x_ = x_;
    previous_y_ = y_;
    previous_theta_ = theta_;
    ticks_old_left_ = current_left;
    ticks_old_right_ = current_right;
}


// PointCloud
void SimpleTurtle::laserScanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
    tf::StampedTransform transform;
     tf::StampedTransform transform2;

    try {
        listener_.lookupTransform("base_footprint", "camera_depth_frame", ros::Time::now(), transform);

    }
    catch(...)
    {

    }

    //ROS_INFO("Transformed message -> x: [%f], y: [%f]", transform.getOrigin().x(), transform.getOrigin().y() );

    for(int i=0; i< msg->ranges.size();i++)
    {
        float range = msg->ranges[i];

        if (isnan(range))
        {
            continue;
        }

        float angle = msg->angle_min + (i*msg->angle_increment);
        tf::Vector3 laser_point(range*cos(angle), range*sin(angle), 0.0);

        try {
            listener_.lookupTransform("odom", "base_footprint", ros::Time::now(), transform2);
        }
        catch(...)
        {

        }

        tf::Vector3 transformedPoint = transform* transform2 * laser_point;

        ROS_INFO("Laser message -> x: [%f], y: [%f]", transformedPoint.getX(), transformedPoint.getY() );


        int map_x;
        int map_y;
        int transformed_map_x;
        int transformed_map_y;

        if(occupancy_grid_map_.worldToMap(x_, y_, map_x, map_y) && occupancy_grid_map_.worldToMap(transformedPoint.getX(), transformedPoint.getY(), transformed_map_x, transformed_map_y))
        {
            Line(map_x,map_y,transformed_map_x,transformed_map_y);

        }



        //Line(x_,y_, transformedPoint.getX(), transformedPoint.getY());


        /*double world_x = transformedPoint.getX();
        double world_y = transformedPoint.getY();
        int map_x;
        int map_y;

        if(occupancy_grid_map_.worldToMap(world_x, world_y, map_x, map_y))
        {
            //ROS_INFO("MAP -> x: [%d], y: [%d]",map_x, map_y);
              occupancy_grid_map_.updateCell(map_x,map_y, 5);

        }*/



            //ROS_INFO("Laser message -> x: [%f], y: [%f]", laser_point.getX(), laser_point.getY() );
            //ROS_INFO("Transformed message -> x: [%f], y: [%f]", transform.getOrigin().x(), transform.getOrigin().y() );

     }

}

/*
void SimpleTurtle::Line( const float x1, const float y1, const float x2, const float y2)
{
    int i,e,x,y,dx,dy;
    int map_x;
    int map_y;
    dx = abs(x2-x1);
    dy = abs(y2-y1);
    x=x1;
    y=y1;
    e = 2*dy-dx;
    i=1;
    do{

        //if(occupancy_grid_map_.worldToMap(x, y, map_x, map_y))
        //{
            // ROS_INFO("MAP -> x: [%d], y: [%d]",map_x, map_y);
            // occupancy_grid_map_.updateCell(map_x,map_y, 5);

        //}
    while(e>=0){
    y++;
    e = e-2*dx;

    int map_x;
    int map_y;

    //if(occupancy_grid_map_.worldToMap(x, y, map_x, map_y))
    //{
       // ROS_INFO("MAP -> x: [%d], y: [%d]",map_x, map_y);
        //  occupancy_grid_map_.updateCell(map_x,map_y, 5);
//
    //}
    }
    x++;
    e = e + 2*dy;
    i++;
    }while(i<=dx);
}*/


void SimpleTurtle::Line(int x0, int y0, int x1, int y1)
{
   int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
   int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1;
   int err = dx+dy, e2; /* error value e_xy */

   for(;;){  /* loop */

       if (x0 != x1 && y0 != y1)
       {
             occupancy_grid_map_.updateCell(x0,y0, 0.1);
       }


      if (x0==x1 && y0==y1)
      {
          occupancy_grid_map_.updateCell(x0,y0, 0.9);
          break;
      }
      e2 = 2*err;
      if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
      if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
   }
}

void SimpleTurtle::CounterClockwisePath()
{
    if(state1_)
    {
         ROS_INFO_STREAM("STATE1" );
        if(x_ <= 1)
        {
            linear_ = 0.2;
            angular_ = 0.0;
        }
        else if(theta_ <= (3.14/2))
        {
            linear_ = 0.0;
            angular_=0.2;
        }
        else
        {
            state1_ = false;
            state2_ = true;
            linear_ = 0.0;
            angular_ = 0.0;
        }
    }
    else if(state2_)
    {
          ROS_INFO_STREAM("STATE2" );
        if(y_ <= 1)
        {
            linear_ = 0.2;
            angular_ = 0.0;
        }
        else if(theta_ <= (3.14))
        {
            linear_ = 0.0;
            angular_=0.2;
        }
        else
        {
            state2_ = false;
            state3_ = true;
            linear_ = 0.0;
            angular_ = 0.0;

        }
    }
    else if(state3_)
    {
         ROS_INFO_STREAM("STATE3" );
        if(x_ >= 0)
        {
            linear_ = 0.2;
            angular_ = 0.0;
        }
        else if(theta_ <= (3*3.14/2))
        {
            linear_ = 0.0;
            angular_=0.2;
        }
        else
        {
            linear_ = 0.0;
            angular_ = 0.0;
            state3_ = false;
            state4_ = true;
        }
    }
    else if(state4_)
    {
          ROS_INFO_STREAM("STATE4" );
        if(y_ >= 0)
        {
            linear_ = 0.2;
            angular_ = 0.0;
        }
        else if(theta_ <= (2*3.14))
        {
            linear_ = 0.0;
            angular_=0.2;
        }
        else
        {
            linear_ = 0.0;
            angular_ = 0.0;
            state4_ = false;

        }
    }
    else
    {
        ROS_INFO_STREAM("FINISHED" );
        ROS_INFO_STREAM("x= " << x_);
        ROS_INFO_STREAM("y= " << y_);
        ROS_INFO_STREAM("theta= " << theta_);
    }
}


void SimpleTurtle::ClockwisePath()
{
    if(state1_)
    {
         ROS_INFO_STREAM("STATE1" );
        if(x_ <= 1)
        {
            linear_ = 0.4;
            angular_ = 0.0;
        }
        else if(theta_ <= (3*3.14/2))
        {
            linear_ = 0.0;
            angular_=0.4;
        }
        else
        {
            state1_ = false;
            state2_ = true;
            linear_ = 0.0;
            angular_ = 0.0;
        }
    }
    else if(state2_)
    {
          ROS_INFO_STREAM("STATE2" );
        if(y_ >= -1)
        {
            linear_ = 0.4;
            angular_ = 0.0;
        }
        else if(theta_ >= (3.14))
        {
            linear_ = 0.0;
            angular_=-0.4;
        }
        else
        {
            state2_ = false;
            state3_ = true;
            linear_ = 0.0;
            angular_ = 0.0;

        }
    }
    else if(state3_)
    {
         ROS_INFO_STREAM("STATE3" );
        if(x_ >= 0)
        {
            linear_ = 0.4;
            angular_ = 0.0;
        }
        else if(theta_ >= (3.14/2))
        {
            linear_ = 0.0;
            angular_=-0.4;
        }
        else
        {
            linear_ = 0.0;
            angular_ = 0.0;
            state3_ = false;
            state4_ = true;
        }
    }
    else if(state4_)
    {
          ROS_INFO_STREAM("STATE4" );
        if(y_ <= 0)
        {
            linear_ = 0.4;
            angular_ = 0.0;
        }
        else if(theta_ >= (0))
        {
            linear_ = 0.0;
            angular_=-0.4;
        }
        else
        {
            linear_ = 0.0;
            angular_ = 0.0;
            state4_ = false;
        }
    }
    else
    {
        ROS_INFO_STREAM("FINISHED" );
        ROS_INFO_STREAM("x= " << x_);
        ROS_INFO_STREAM("y= " << y_);
        ROS_INFO_STREAM("theta= " << theta_);
    }
}

// step function
// called with 10Hz
void SimpleTurtle::step()
{
    if(button_0_pressed_)
    {
        // MAKE YOUR CHANGES HERE

        //ClockwisePath();

        CounterClockwisePath();

        pubCmdVel(linear_,angular_);

    }
}

} // end namespace SimpleTurtle

int main(int argc, char **argv)
{
    ros::init(argc, argv, "simple_turtle");

    SimpleTurtle::SimpleTurtle simple_turtle;

    simple_turtle.init();

    // main loop at 10Hz
    ros::Rate loop_rate(10);
    while (ros::ok())
    {
        simple_turtle.step();

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}
